package org.madrien;

import java.nio.file.*;
import java.io.*;
import java.nio.charset.*;
import java.util.*;

public class ReverseLineReader {
	public static void main(String[] args) {
		Path path = Paths.get("file.txt");
		StringBuilder sb = new StringBuilder();
		String currentLine = null;
		List<String> linesRead = new ArrayList<>();
		
		try (BufferedWriter writer = Files.newBufferedWriter(path, Charset.defaultCharset());
			 BufferedReader reader = Files.newBufferedReader(path, Charset.defaultCharset())) {
			writer.write("This tomato is delicious!");
			writer.newLine();
			writer.write("And that meat is so tender!");
			writer.flush();
			
			linesRead = Files.readAllLines(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
		List<String> linesReversed = new ArrayList<>();;
		for (int i = linesRead.size()-1; i >= 0; i--) {
			linesReversed.add(linesRead.get(i));
		}
		System.out.print(linesReversed);
	}
}