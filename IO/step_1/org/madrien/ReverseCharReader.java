package org.madrien;

import java.nio.file.*;
import java.io.*;
import java.nio.charset.*;

public class ReverseCharReader {
	public static void main(String[] args) {
		Path path = Paths.get("file.txt");
		StringBuilder sb = new StringBuilder();
		String currentLine = null;
		
		try (BufferedWriter writer = Files.newBufferedWriter(path, Charset.defaultCharset());
			 BufferedReader reader = Files.newBufferedReader(path, Charset.defaultCharset())) {
			writer.write("This tomato is delicious!");
			writer.newLine();
			writer.write("And that meat is so tender!");
			writer.flush();
			
			while ((currentLine = reader.readLine()) != null) { 
				sb.append(currentLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		sb.reverse();
		System.out.print(sb);
	}
}