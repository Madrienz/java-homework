package org.madrien;

import java.nio.file.*;
import java.io.*;
import java.nio.charset.*;
import java.util.*;

public class DataStreamTest {
    public static void main(String[] args) {
        Path path = Paths.get("data.txt");
        String nameRead = "";
        float priceRead = 0;
        int quantityRead = 0;
        try (DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path.toFile())));
             DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(path.toFile())))) {
                out.writeChars("Pack of tea    ");  //name (15 chars per name)
                out.writeFloat(3.00f);              //price
                out.writeInt(253);                  //quantity
				
                out.writeChars("Bread          ");	
                out.writeFloat(2.50f);			
                out.writeInt(50);				
				
                out.writeChars("Sausage        ");			
                out.writeFloat(5.25f);			
                out.writeInt(146);
                out.flush();
				
                for (int i = 0; i < 15; i++) {
                    nameRead = nameRead + in.readChar();
                }
                priceRead = in.readFloat();
                quantityRead = in.readInt();
                System.out.println("Name: "+nameRead+", Price: "+priceRead+", Quantity: "+quantityRead);
				
                nameRead = "";
                for (int i = 0; i < 15; i++) {
                    nameRead = nameRead + in.readChar();
                }
                priceRead = in.readFloat();
                quantityRead = in.readInt();
                System.out.println("Name: "+nameRead+", Price: "+priceRead+", Quantity: "+quantityRead);
				
                nameRead = "";
                for (int i = 0; i < 15; i++) {
                    nameRead = nameRead + in.readChar();
                }
                priceRead = in.readFloat();
                quantityRead = in.readInt();
                System.out.println("Name: "+nameRead+", Price: "+priceRead+", Quantity: "+quantityRead);
        } catch (EOFException e) {
            System.out.println("Reached the end of file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}