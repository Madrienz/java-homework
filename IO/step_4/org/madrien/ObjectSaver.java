package org.madrien;

import java.nio.file.*;
import java.io.*;
import java.nio.charset.*;
import java.util.*;

public class ObjectSaver {
	public static void main(String[] args) {
		Path path = Paths.get("objects.txt");
		if(!Files.exists(path)) {
			try { Files.createFile(path); }
			catch (IOException e) { e.printStackTrace(); }
		}
		ObjectContainer obj1 = new ObjectContainer();
		ObjectContainer obj2 = new ObjectContainer();
		ObjectContainer resultObj1 = null;
		ObjectContainer resultObj2 = null;
		Outer outer = new Outer();
		obj1.outer = outer;
		obj2.outer = outer;
		System.out.println("Before serialization: Are references the same? " + (obj1.outer == obj2.outer));
		
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("objects.txt"));
			 ObjectInputStream in = new ObjectInputStream(new FileInputStream("objects.txt"))) {
				out.writeObject(obj1);
				out.writeObject(obj2);
				out.flush();
				
				System.out.println("Objects have been serialized.");
				
				resultObj1 = (ObjectContainer) (in.readObject());
				resultObj2 = (ObjectContainer) (in.readObject());
				System.out.println("Objects have been deserialized.");
		} catch (EOFException e) {
			System.out.println("Reached the end of file.");
			e.printStackTrace();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("After serialization: Are references the same? " + (resultObj1.outer == resultObj2.outer));
	}
}

class ObjectContainer implements Serializable {
	public Outer outer;
}
	
class Outer implements Serializable {
	private Middle middle = new Middle();
}

class Middle implements Serializable {
	private Inner inner = new Inner();
}

class Inner implements Serializable {}