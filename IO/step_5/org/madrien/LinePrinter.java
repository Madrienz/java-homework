package org.madrien;

import java.nio.file.*;
import java.io.*;
import java.nio.charset.*;
import java.util.*;

public class LinePrinter {
	public static void main(String[] args) {
		Path path = null;
		Console console = System.console();
		if(console != null) {
			System.out.println("Please enter the file path.");
			path = getExistingPath(console);
			
			System.out.println("System is ready to read the file. Press Enter to read one line.");
			String userInput = "";
			String currentLine = null;
				
			try (BufferedReader reader = Files.newBufferedReader(path)) {
				while(!userInput.equals("q")){
					if((currentLine = reader.readLine()) != null) {
						System.out.println(currentLine);
						userInput = console.readLine();
					} else {
						System.out.println("End of file.");
						break;
					}
				} 
			} catch (MalformedInputException e) {
				System.out.println("This file does not contain readable for human strings.");	
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Path getExistingPath(Console console) {
		Path path = Paths.get(console.readLine());
		while (!Files.exists(path) || !Files.isRegularFile(path)) {
			System.out.println("No such file exist. Please choose another file.");
			path = Paths.get(console.readLine());
		}
		return path;
	}
}