package org.madrien;

public class MyOrderFactory implements OrderFactory {
	public Order create(OrderStatus status) {
		return new Order(status);
	}
	
	public static void main(String[] args) {
		MyOrderFactory factory = new MyOrderFactory();
		Order a = factory.createNotStarted();
		Order b = factory.createProcessing();
		Order c = factory.createCompleted();
		Order d = factory.create(OrderStatus.PROCESSING);
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
	}
}