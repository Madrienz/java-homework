package org.madrien;

public interface OrderFactory {
	default public Order createNotStarted() {
		return create(OrderStatus.NOT_STARTED);
	}
	
	default public Order createProcessing() {
		return create(OrderStatus.PROCESSING);
	}
	
	default public Order createCompleted() {
		return create(OrderStatus.COMPLETED);
	}
	
	public Order create(OrderStatus status);		
}

enum OrderStatus {
	NOT_STARTED, PROCESSING, COMPLETED
}

class Order {
	public final OrderStatus status; 
	
	public Order(OrderStatus status) {
		this.status = status;
    }

    public OrderStatus getStatus() {
        return status;
    }
	
	public String toString() {
		return status.toString();
	}
}