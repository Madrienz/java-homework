package org.madrien;

import java.util.List;
import java.util.ArrayList;
import java.util.stream.Stream;
import java.util.stream.Collectors;

public class FilterOrders {
    public static void main(String[] args) {
		
		//create a list and fill it with completed orders with price < 5000$
		List<Order> list = new ArrayList<>();
		for(int i=0; i<100; i++) {
			list.add(new Order(OrderStatus.COMPLETED, (int) (Math.random() * 5000)));
		}
		
		List<Order> filteredList = list.stream()
			.filter(Order::isCheap)
			.collect(Collectors.toList());
		System.out.println(filteredList);
    }
    
    public enum OrderStatus {
        NOT_STARTED, PROCESSING, COMPLETED
    }

    public static class Order {
        public final OrderStatus status; 
        private int price;
        
        public Order(OrderStatus status, int price) {
            this.status = status;
            this.price = price;
        }
        
        
        public OrderStatus getStatus() {
            return status;
        }
        
        public int getPrice() {
            return price;
        }
		
        public String toString() {
            return price + "";
        }
		
        public boolean isCheap() {
            return (price < 500);
        }
    }
}