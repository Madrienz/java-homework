I think the only logical use of an Optional as input parameter is for method, that 
supposed to somehow process Optional argument. Otherwise, for optional (not nessessary) parameters
there are varargs and method overloading, and for general parameters Optional would do
unnessassary wrapping that comes with a cost. If we want to build some logic based on 
the value of the input parameter, or check for null values we can do it without using an Optional.