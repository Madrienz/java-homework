package org.madrien;

public class TestOrderProcessor {
	public static void main(String[] args) {
		//testing Order
		Order order = new Order(OrderStatus.NOT_STARTED);
		Order orderDone = OrderProcessor.process(order);
		if(orderDone.getStatus() == OrderStatus.COMPLETED) {
			System.out.println("Order is processed.");
		}
		
		//testing order subclass
		Order orderChild = new OrderChild();
		orderDone = OrderProcessor.process(orderChild);
		if(orderDone.getStatus() == OrderStatus.COMPLETED) {
			System.out.println("Order is processed.");
		}
		
		//testing order subclass with already processing state
		orderChild = new OrderChild(OrderStatus.PROCESSING);
		orderDone = OrderProcessor.process(orderChild);
		if(orderDone.getStatus() == OrderStatus.COMPLETED) {
			System.out.println("Order is processed.");
		}
	}
}

class OrderChild extends Order {
	public OrderChild() {
		super();
	}
	
	public OrderChild(OrderStatus status) {
		super(status);
	}
}