package org.madrien;

public class OrderProcessor {
	
	/** static method for processing Orders and its subclasses */
	public static <T extends Order> Order process(T order) {
		if(order.getStatus() != OrderStatus.NOT_STARTED)
			System.out.println("The order is being processed or has been processed.");
		else {
			//process order...
			order.setStatus(OrderStatus.COMPLETED);
		}
		return order;
	}
}

enum OrderStatus {
        NOT_STARTED, PROCESSING, COMPLETED
}

class Order {
        protected OrderStatus status;

        /** default constructor with NOT_STARTED status */
		public Order() {
			this(OrderStatus.NOT_STARTED);
		}
		
		public Order(OrderStatus status) {
            this.status = status;
        }

        public OrderStatus getStatus() {
            return status;
        }
		
		public void setStatus(OrderStatus status) {
			this.status = status;
		}
    }