package org.madrien;

import org.madrien.food.*;
import org.madrien.kitchen.*;
import org.madrien.order.*;

public class Waiter {
	/** method for processing client's order. Activates overriden carry() method from Order interface */ 
	public void processOrder(Order order) {
		System.out.println("Waiter: Accepted order...");
		order.carry();
	}
	
	public void deliverFood(Food food) {
		System.out.println("Waiter: Delivering food...");
		food.deliver();
	}
}