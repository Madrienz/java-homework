package org.madrien;

import org.madrien.food.*;
import org.madrien.kitchen.*;
import org.madrien.order.*;

/* It works like this: 
	Client uses orderPizza() and orderDrink() methods for creating PizzaOrder and DrinkOrder respectively 
	and passing them to the Waiter by calling his processOrder(Order order) method. It accepts any object 
	that implements Order interface, so we can add many more different concrete orders and Waiter will work 
	just fine. 
	Any class that implements Order has to override it's method carry(). By doing that we show every order 
	who should it be passed to next. We pass PizzaOrder to Kitchen which decides who will make pizza: PizzaMaker
	or PizzaShop. We pass DrinkOrder to Barman. 
	Order uses carry() method that calls createPizza() or makeDrink() methods of Kitchen and Barman and passes 
	the name of item ordered. When they get the name, they make Food, which works just like Orders, and call the
	deliverFood() method of Waiter. This method calls deliver() method of Food.
 */

public class RestaurantTest {
	public static void main(String[] args) {
		Waiter waiter = new Waiter();
		Client client = new Client();
		Kitchen kitchen = new Kitchen(waiter, client);
		Barman barman = new Barman(waiter, client);
		System.out.println("Is client happy: " + client.isHappy());
		client.orderPizza("Pepperoni", waiter, kitchen);
		client.orderDrink("Sprite", waiter, barman);
		
		System.out.println("Is client happy: " + client.isHappy());
		System.out.println("Pizza: " + client.getPizzaSlot());
		System.out.println("Drink: " + client.getDrinkSlot());
		
		//order pizza from PizzaShop, because PizzaMaker can not make it
		client.orderPizza("Bavarian", waiter, kitchen);
		System.out.println("Is client happy: " + client.isHappy());
		System.out.println("Pizza: " + client.getPizzaSlot());
		System.out.println("Drink: " + client.getDrinkSlot());
	}
}