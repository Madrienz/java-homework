package org.madrien.kitchen;

import org.madrien.*;
import org.madrien.food.*;

public class PizzaShop {
	public void sellPizza(String pizzaName, Waiter waiter, Client client) {
		System.out.println("PizzaShop: Selling pizza...");
		waiter.deliverFood(new Pizza(pizzaName, client));
	}
}