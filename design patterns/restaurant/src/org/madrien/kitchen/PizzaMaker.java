package org.madrien.kitchen;

import java.util.List;
import java.util.ArrayList;
import org.madrien.*;
import org.madrien.food.*;

public class PizzaMaker {	
	List<String> listOfAvailablePizzas = List.of("Pepperoni", "Margaerita", "Four Cheeses");
	
	public boolean canCook(String pizzaName) {
		if(listOfAvailablePizzas.contains(pizzaName)) {
			return true;
		} else {
			return false;
		}
	}
	
	public void cookPizza(String pizzaName, Waiter waiter, Client client) {
		System.out.println("PizzaMaker: Making pizza...");
		waiter.deliverFood(new Pizza(pizzaName, client));
	}
}