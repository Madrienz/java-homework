package org.madrien.kitchen;

import org.madrien.*;

public class Kitchen {
	private PizzaMaker pizzaMaker = new PizzaMaker();
	private PizzaShop pizzaShop = new PizzaShop();
	private Waiter waiter;
	private Client client;
	
	public Kitchen(Waiter waiter, Client client) {
		this.waiter = waiter;
		this.client = client;
	}
	
	public void createPizza(String pizzaName) {
		System.out.println("Kitchen: Processing order...");
		if(pizzaMaker.canCook(pizzaName)) {
			pizzaMaker.cookPizza(pizzaName, waiter, client);
		} else {
			pizzaShop.sellPizza(pizzaName, waiter, client);
		}
	}
}