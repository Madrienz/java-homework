package org.madrien.food;

import org.madrien.*;

public class Pizza implements Food {
	private String name;
	private Client client;
		
	public Pizza(String name, Client client) {
		this.name = name;
		this.client = client;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}
	
	public void deliver() {
		client.acceptPizza(this);
	}
}