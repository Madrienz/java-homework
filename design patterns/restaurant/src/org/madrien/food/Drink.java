package org.madrien.food;

import org.madrien.*;

public class Drink implements Food {
	private String name;
	private Client client;
	
	public Drink(String name, Client client) {
		this.name = name;
		this.client = client;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}
	
	public void deliver() {
		client.acceptDrink(this);
	}
}