package org.madrien;

import org.madrien.food.*;
import org.madrien.kitchen.*;
import org.madrien.order.*;

public class Client {
	private Pizza pizzaSlot = null;
	private Drink drinkSlot = null;
	
	public Pizza getPizzaSlot() {
		return pizzaSlot;
	}
	
	public Drink getDrinkSlot() {
		return drinkSlot;
	}
		
	public boolean isHappy() {
		return (pizzaSlot != null && drinkSlot != null);
	}
		
	/** methods for ordering. */	
	public void orderPizza(String pizzaName, Waiter waiter, Kitchen kitchen) {
		System.out.println("Client: Ordering a pizza...");
		waiter.processOrder(new PizzaOrder(pizzaName, kitchen));
	}
		
	public void orderDrink(String drinkName, Waiter waiter, Barman barman) {
		System.out.println("Client: Ordering a drink...");
		waiter.processOrder(new DrinkOrder(drinkName, barman));
	}
	
	public void acceptPizza(Pizza pizza) {
		System.out.println("Client: Accepting a pizza...");
		pizzaSlot = pizza;
	}
	
	public void acceptDrink(Drink drink) {
		System.out.println("Client: Accepting a drink...");
		drinkSlot = drink;
	}
}
	