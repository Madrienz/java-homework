package org.madrien;

import org.madrien.food.*;

public class Barman {
	private Waiter waiter;
	private Client client;
	
	public Barman(Waiter waiter, Client client) {
		this.waiter = waiter;
		this.client = client;
	}
	
	public void makeDrink(String drinkName) {
		System.out.println("Barman: Making drink...");
		waiter.deliverFood(new Drink(drinkName, client));
	}
}