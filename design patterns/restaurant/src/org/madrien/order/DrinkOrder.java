package org.madrien.order;

import org.madrien.*;

public class DrinkOrder implements Order {
	private String drinkName;
	private Barman barman;
	
	public DrinkOrder(String drinkName, Barman barman) {
		this.drinkName = drinkName;
		this.barman = barman;
	}
	
	public String getDrinkName() {
		return drinkName;
	}
	
	@Override
	public void carry() {
		barman.makeDrink(drinkName);
	}
}