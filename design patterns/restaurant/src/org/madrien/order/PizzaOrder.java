package org.madrien.order;

import org.madrien.*;
import org.madrien.kitchen.*;

public class PizzaOrder implements Order {
	private String pizzaName;
	private Kitchen kitchen;
	
	public PizzaOrder(String pizzaName, Kitchen kitchen) {
		this.pizzaName = pizzaName;
		this.kitchen = kitchen;
	}
	
	public String getPizzaName() {
		return pizzaName;
	}
	
	@Override
	public void carry() {
		kitchen.createPizza(pizzaName);
	}
}