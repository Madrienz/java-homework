package org.madrien;

import org.madrien.payment.Payment;

class Client {
	private int money;
	private String phoneNum;
	
	public Client(int money, String phoneNum) {
		this.money = money;
		this.phoneNum = phoneNum;
	}
	
	public int getMoney() {
		return money;
	}
	
	public String getPhoneNum() {
		return phoneNum;
	}
	
	/** method for paying */
	public void pay(Terminal terminal, Payment payment) {
		System.out.println("Paying " + payment.getMoneyGiven() + "$ for " + payment.getOrderPrice() + "$ order...");
		if(payment.getMoneyGiven() > money) {
			throw new IllegalArgumentException("Client does not have that much money!");
		}
		money -= payment.getMoneyGiven();
		money += terminal.acceptPayment(payment); //get cashback
	}
}