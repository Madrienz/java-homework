package org.madrien;

import org.madrien.payment.Payment;

class Terminal {
	/** method for processing transaction from client */
	public int acceptPayment(Payment payment) {
		System.out.println("Terminal: Accepting payment...");
		int bonus = payment.calculateBonus();
		int moneyToReturn = payment.calculateCashBack() + bonus;
		System.out.println("Terminal: Returned " + moneyToReturn + "$!");
		return moneyToReturn;
	}
}