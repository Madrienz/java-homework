package org.madrien.payment;

public class StandartBonus extends BonusProgramDecorator {
	public StandartBonus(Payment wrappee, String phoneNum) {
		super(wrappee, phoneNum);
	}
	
	@Override
	public int calculateCashBack() {
		return super.calculateCashBack();
	}
	
	@Override
	public int calculateBonus() {
		System.out.println("Using standart bonus.");
		return super.calculateBonus();
	}
}