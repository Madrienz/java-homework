package org.madrien.payment;

public interface Payment {
	int getOrderPrice();
	int getMoneyGiven();
	int calculateCashBack();
	int calculateBonus();
}