package org.madrien.payment;

public class Card implements Payment {
	private int orderPrice;
	private int moneyGiven;
	
	public Card(int orderPrice) {
		this.orderPrice = orderPrice;
		this.moneyGiven = orderPrice;
	}
	
	public int getOrderPrice() {
		return orderPrice;
	}
	
	public int getMoneyGiven() {
		return moneyGiven;
	}
	
	@Override
	public int calculateCashBack() {
		return 0;
	}
	
	public int calculateBonus() {
		return 0;
	}
}