package org.madrien.payment;

public class PremiumBonus extends BonusProgramDecorator {
	public PremiumBonus(Payment wrappee, String phoneNum) {
		super(wrappee, phoneNum);
	}
	
	@Override
	public int calculateCashBack() {
		return super.calculateCashBack();
	}
	
	@Override
	public int calculateBonus() {
		System.out.println("Using premium bonus.");
		int usedBonus = super.calculateBonus();
		int record = BonusProgram.getBonusRecords().get(phoneNum);
		record += (usedBonus * 10) / 2;
		BonusProgram.getBonusRecords().put(phoneNum, record);
		System.out.println("Returned half of used bonuses.");
		return usedBonus;
	}
}