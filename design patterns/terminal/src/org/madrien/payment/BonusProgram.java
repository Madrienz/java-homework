package org.madrien.payment;

import java.util.Map;
import java.util.HashMap;

public class BonusProgram {
	/** map for storing records for bonus program, Strings are phone numbers, Integers are bonus points */ 
	private static Map<String, Integer> bonusProgramRecords = new HashMap<>();
	
	public static Map<String, Integer> getBonusRecords() {
		return bonusProgramRecords;
	}
	
	/** utility method for filling bonus records map with values */
	public static void fillBonusRecords(Map<String, Integer> inputMap) {
		bonusProgramRecords.putAll(inputMap);
	}
}