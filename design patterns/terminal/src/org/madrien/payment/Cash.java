package org.madrien.payment;

public class Cash implements Payment {
	private int orderPrice;
	private int moneyGiven;
	
	public Cash(int orderPrice, int moneyGiven) {
		this.orderPrice = orderPrice;
		this.moneyGiven = moneyGiven;
	}
	
	public int getOrderPrice() {
		return orderPrice;
	}
	
	public int getMoneyGiven() {
		return moneyGiven;
	}
	
	@Override
	public int calculateCashBack() {
		if(orderPrice > moneyGiven) {
			throw new IllegalArgumentException("Not enough money given!");
		}
		System.out.println("Cashback is " + (moneyGiven - orderPrice) + "$.");
		return (moneyGiven - orderPrice);
	}
	
	public int calculateBonus() {
		return 0;
	}
}