package org.madrien.payment;

class BonusProgramDecorator implements Payment {
	protected Payment wrappee;
	protected String phoneNum;
	
	public BonusProgramDecorator(Payment wrappee, String phoneNum) {
		this.wrappee = wrappee;
		this.phoneNum = phoneNum;
	}
	
	@Override
	public int calculateCashBack() {
		return wrappee.calculateCashBack();
	}
	
	public int getOrderPrice() {
		return wrappee.getOrderPrice();
	}
	
	public int getMoneyGiven() {
		return wrappee.getMoneyGiven();
	}
	
	public int calculateBonus() {
		System.out.println("Calculating bonus from " + wrappee.getOrderPrice() + "$ order...");
		int usedBonus = 0;
		int pointsReturned = 0;
		if(BonusProgram.getBonusRecords().containsKey(phoneNum)) {
			int record = BonusProgram.getBonusRecords().get(phoneNum);
			System.out.println("Client's bonuses: " + record);
			if((record / 10) >= wrappee.getOrderPrice() && record > 10) {
				usedBonus = wrappee.getOrderPrice();
				record -= wrappee.getOrderPrice() * 10;
			} else if(record > 10) {
				usedBonus = record / 10;
				record = record % 10;
			}
			pointsReturned = (wrappee.getOrderPrice() - usedBonus) / 10;
			record += pointsReturned;
			BonusProgram.getBonusRecords().replace(phoneNum, record);
		} else {
			System.out.println("No such number in bonus program records. This number is now added to the database.");
			BonusProgram.getBonusRecords().put(phoneNum, wrappee.getOrderPrice() / 10);
		}
		System.out.println("Bonus is " + usedBonus + "$.");
		return usedBonus;
	}
}