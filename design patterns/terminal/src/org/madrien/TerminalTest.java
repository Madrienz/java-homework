package org.madrien;

import org.madrien.payment.*;

import java.util.Map;
import java.util.HashMap;

/* It works like this: 
	Client has pay() method, that accepts 
	- Terminal object that will be used to process payment,
	- object that implements Payment interface that will show the terminal how to process it correctly, and
	- the phone number as String.
	Any object implementing Payment interface has to override calculateCashBack() method that returns an int
	showing how much money Client will get back in case of overpaying. By using Payment interface we can later add
	other payment methods for Client and Terminal without changing their paying logic.
	pay() method calculates how much money Client has given away and calls acceptPayment() method of 
	Terminal passing references to objects above to Terminal object.
	acceptPayment() method calls static calculateBonus() method from BonusProgram, calls calculateCashBack() of 
	incoming Payment object, and returns the sum to the Client, which he adds to his money field.
	We use Decorator pattern for adding bonus program to payment method. We wrap cash or card (or any possible future
	class) in StandartBonus or PremiumBonus and so the payment method can calculate bonuses. 
	calculateBonus() logic: 
	We add 1 point per 10$ paid to the bonus wallet, and remove 10 points per 1$ taken away from the order price.
	If Client has enough points to cover the whole cost of the order, we remove orderPrice*10 from his bonus wallet 
	and no money is taken from the Client. 
	If we can cover some part of the order price with bonus points, we take (points % 10)*10 from Client's wallet and
	the rest is paid with actual money. The amount paid with money is divided by 10 and added to the bonus wallet.
	If we have no bonus points left (less than 10) whole order price is paid with money with the adding logic above.
 */

public class TerminalTest {
	public static void main(String[] args) {
		Terminal terminal = new Terminal();
		Map<String, Integer> map = new HashMap<>();
		map.put("243-864-2341", 6000);
		map.put("345-236-2181", 400);
		map.put("353-242-8185", 250);
		BonusProgram.fillBonusRecords(map);
		Client client = new Client(1000, "353-242-8185");
		
		//purchase with bonuses enough to cover all cost
		System.out.println("Client's money: " + client.getMoney() + "$.");
		client.pay(terminal, new StandartBonus(new Cash(20,25), client.getPhoneNum()));
		System.out.println("Client's money: " + client.getMoney() + "$.");
		System.out.println("Client's bonuses: " + BonusProgram.getBonusRecords().get(client.getPhoneNum()));
		System.out.println();
		
		//purchase with some bonuses
		System.out.println("Client's money: " + client.getMoney() + "$.");
		client.pay(terminal, new PremiumBonus(new Card(10), client.getPhoneNum()));
		System.out.println("Client's money: " + client.getMoney() + "$.");
		System.out.println("Client's bonuses: " + BonusProgram.getBonusRecords().get(client.getPhoneNum()));
		System.out.println();
		
		//purchase with 0 bonuses
		System.out.println("Client's money: " + client.getMoney() + "$.");
		client.pay(terminal, new PremiumBonus(new Card(300), client.getPhoneNum()));
		System.out.println("Client's money: " + client.getMoney() + "$.");
		System.out.println("Client's bonuses: " + BonusProgram.getBonusRecords().get(client.getPhoneNum()));
		System.out.println();
		
		//purchase with wrong phone number
		System.out.println("Client's money: " + client.getMoney() + "$.");
		client.pay(terminal, new StandartBonus(new Cash(250, 265), "545-824-9892"));
		System.out.println("Client's money: " + client.getMoney() + "$.");
		System.out.println("Client's bonuses: " + BonusProgram.getBonusRecords().get("545-824-9892"));
		System.out.println();
	}
}