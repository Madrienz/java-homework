The `object` variable will be null after the `try` block because of the order of assignment.
The expression on the right side of `=` operator is evaluated first, and then it is 
assigned to the variable. Since we got exception in the first step, assignment never happens.