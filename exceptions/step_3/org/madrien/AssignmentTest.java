package org.madrien;

public class AssignmentTest {
	public static void main(String[] args) {
		Uninstantiable object = null;
		try {
			object = new Uninstantiable();
		} catch (RuntimeException e) {
			System.out.println(object);
		}
	}
}

class Uninstantiable {
	public Uninstantiable() {
		throw new RuntimeException("in constructor");
	}
	
	public String toString() {
		return "Object";
	}
}