package org.madrien;

public class BrandNewCustomMadeException extends Exception {
	public BrandNewCustomMadeException(String message) {
		super(message);
	}
	
	public BrandNewCustomMadeException(String message, Throwable cause) {
		super(message, cause);
	}
}