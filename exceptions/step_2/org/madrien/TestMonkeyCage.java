package org.madrien;

public class TestMonkeyCage {
	public static void main(String[] args) {
		try (MonkeyCage cage = new MonkeyCage()) {
			//do something
		}
		System.out.println("The end.");
	}
}	


class MonkeyCage implements AutoCloseable {
	public MonkeyCage() {
		System.out.println("The cage is opened.");
	}
	
	public void close() {
		System.out.println("The cage has been closed.");
	}
}

