package org.madrien;

import org.reflections.Reflections;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AnnotationFinder {
    /** method for finding objects, that instantiate deprecated classes (Step 1)*/
    public static Set<Object> findDeprecated(List<Object> list) {
        if (list == null || list.isEmpty())
            throw new IllegalArgumentException();
        Set<Object> resultSet = new HashSet<>();
        for (Object obj : list) {
            if (obj.getClass().isAnnotationPresent(Deprecated.class)) {
                resultSet.add(obj);
            }
        }
        return resultSet;
    }

    /** method for finding objects, that instantiate deprecated classes
     * and suggesting to use other subtypes of the superclass of the object found (Step 2)*/
    public static Set<Object> findDeprecatedWithSuggestions(List<Object> list) {
        Set<Object> resultSet = findDeprecated(list);
        Reflections reflections = new Reflections("org.madrien");
        Class objSuperclass = null;
        Set<Class> subTypes = null;
        if (!resultSet.isEmpty()) {
            for (Object obj : resultSet) {
                objSuperclass = obj.getClass().getSuperclass();
                subTypes = reflections.getSubTypesOf(objSuperclass); //org.reflections method
                System.out.println("Possible non-deprecated options for " + obj.getClass());
                for (Class c : subTypes) {
                    if (!c.isAnnotationPresent(Deprecated.class)) {
                        System.out.println(c);
                    }
                }
            } //end of outer for loop
            return resultSet;
        }
        return resultSet;
    }
}