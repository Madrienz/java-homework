package org.madrien;

import java.util.ArrayList;
import java.util.List;

public class TestFinder {
    public static void main(String[] args) {
        List<Object> list = new ArrayList<>();
        for(int i=0; i<10; i++) {
            int x = (int) (Math.random() * 3);
            switch(x) {
                case 0: list.add(new A()); break;
                case 1: list.add(new B()); break;
                case 2: list.add(new C()); break;
                default: throw new IllegalArgumentException();
            }
        }
        System.out.println(list);
        System.out.println(AnnotationFinder.findDeprecated(list));
        System.out.println(AnnotationFinder.findDeprecatedWithSuggestions(list));
    }
}

class A {
    @Override
    public String toString() {
        return "A";
    }
}

/**
 * @deprecated
 * do not use
 */
@Deprecated
class B extends D {
    @Override
    public String toString() {
        return "B";
    }
}

class C {
    @Override
    public String toString() {
        return "C";
    }
}

class D {
    @Override
    public String toString() {
        return "D";
    }
}

class E extends D {
    @Override
    public String toString() {
        return "E";
    }
}

class F extends D {
    @Override
    public String toString() {
        return "F";
    }
}