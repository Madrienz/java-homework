package org.madrien;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.spy;

@PowerMockIgnore({"com.sun.org.apache.xerces.*", "javax.xml.*", "org.xml.*", "org.w3c.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({NumberRowSupplier.class})
public class CalculatorTestPositive {
    @Test
    public void testPlus() {
        assertEquals(11, Calculator.plus(8, 3));
    }

    @Test
    public void testPlusWithNegative() {
        assertEquals(-18, Calculator.plus(-15, -3));
    }

    @Test
    public void testPlusWithPositiveAndNegative() {
        assertEquals(5,Calculator.plus(-25, 30));
    }

    @Test
    public void testMinus() {
        assertEquals(98, Calculator.minus(153, 55));
    }

    @Test
    public void testMinusWithNegative() {
        assertEquals(-5, Calculator.minus(-53, -48));
    }

    @Test
    public void testMinusWithPositiveAndNegative() {
        assertEquals(-50, Calculator.minus(-16, 34));
    }

    @Test
    public void testMultiply() {
        assertEquals(72, Calculator.multiply(8, 9));
    }

    @Test
    public void testMultiplyWithNegative() {
        assertEquals(160, Calculator.multiply(-32, -5));
    }

    @Test
    public void testMultiplyWithPositiveAndNegative() {
        assertEquals(-150, Calculator.multiply(15, -10));
    }

    @Test
    public void testDivide() {
        assertEquals(9, Calculator.divide(63, 7), 0.0001);
    }

    @Test
    public void testDivideWithNegative() {
        assertEquals(10.2857, Calculator.divide(-72, -7), 0.0001);
    }

    @Test
    public void testDivideWithPositiveAndNegative() {
        assertEquals(-8.5, Calculator.divide(153, -18), 0.0001);
    }

    @Test
    public void testSqrt() {
        assertEquals(12, Calculator.sqrt(144), 0.0001);
    }

    @Test
    public void testSqrtWithDouble() {
        assertEquals(12.9228, Calculator.sqrt(167), 0.0001);
    }

    @Test(timeout = 500)
    public void testIsPrime() {
        assertTrue(Calculator.isPrime(541));
        assertFalse(Calculator.isPrime(64));
        assertFalse(Calculator.isPrime(15));
    }

    @Test
    public void testFibonacciNum() {
        assertEquals(233, Calculator.fibonacciNum(13));
    }

    @Test
    public void testSummarizeNumberRow() {
        assertEquals(15, Calculator.summarizeNumberRow(NumberRowSupplier.return456()));
    }

    @Test
    public void testSummarizeNumberRowWithMockito() {
        NumberRowSupplier supplier = mock(NumberRowSupplier.class);
        Mockito.when(supplier.returnArray()).thenReturn(new int[] {44,55,66});
        assertEquals(165, Calculator.summarizeNumberRow(supplier.returnArray()));
    }

    @Test
    public void testSummarizeNumberRowPrivate() throws Exception {
        NumberRowSupplier supplier = spy(new NumberRowSupplier());
        PowerMockito.when(supplier, "return789").thenReturn(new int[] {77,88,99});

        int result = Calculator.summarizeNumberRow(supplier.callPrivateReturn789());
        PowerMockito.verifyPrivate(supplier).invoke("return789");

        assertEquals(264, result);
    }

    @Test
    public void testSummarizeNumberRowStatic() {
        PowerMockito.mockStatic(NumberRowSupplier.class);
        Mockito.when(NumberRowSupplier.return456()).thenReturn(new int[] {44,55,66});

        int result = Calculator.summarizeNumberRow(NumberRowSupplier.return456());
        PowerMockito.verifyStatic(NumberRowSupplier.class);
        NumberRowSupplier.return456();

        assertEquals(165, result);
    }

    @Test
    public void testSummarizeNumberRowFinal() {
        NumberRowSupplier supplier = spy(new NumberRowSupplier());
        Mockito.when(supplier.return123()).thenReturn(new int[] {11,22,33});

        int result = Calculator.summarizeNumberRow(supplier.return123());
        Mockito.verify(supplier).return123();

        assertEquals(66, result);
    }
}
