package org.madrien;

import org.junit.Test;

public class CalculatorTestNegative {
    @Test(expected = IllegalArgumentException.class)
    public void testDivide() {
        Calculator.divide(8, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSqrt() {
        Calculator.sqrt(-50);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFibonacciNum() {
        Calculator.fibonacciNum(-3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSummarizeNumberRow() {
        Calculator.summarizeNumberRow(new int[0]);
    }
}
