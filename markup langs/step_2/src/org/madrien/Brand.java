package org.madrien.serialization;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Brand {

    @SerializedName("BrandName")
    private String brandName;
	
    @SerializedName("Branch")
    private List<Branch> branch = null;

    public String getBrandName() {
        return brandName;
    }

    public List<Branch> getBranch() {
        return branch;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
