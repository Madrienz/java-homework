package org.madrien.serialization;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StandardAvailability {

    @SerializedName("Day")
    private List<Day> day = null;

    public List<Day> getDay() {
        return day;
    }
}
