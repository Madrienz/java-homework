package org.madrien.serialization;

import com.google.gson.annotations.SerializedName;

public class OtherServiceAndFacility {

    @SerializedName("Code")
    private String code;
	
    @SerializedName("Name")
    private String name;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
