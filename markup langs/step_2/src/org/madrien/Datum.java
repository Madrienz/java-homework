package org.madrien.serialization;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("Brand")
    private List<Brand> brand = null;

    public List<Brand> getBrand() {
        return brand;
    }
}
