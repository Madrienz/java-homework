package org.madrien.serialization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpeningHour {

    @SerializedName("OpeningTime")
    private String openingTime;
	
    @SerializedName("ClosingTime")
    private String closingTime;

    public String getOpeningTime() {
        return openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }
}
