package org.madrien.serialization;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Day {

    @SerializedName("Name")
    private String name;
	
    @SerializedName("OpeningHours")
    private List<OpeningHour> openingHours = null;

    public String getName() {
        return name;
    }

    public List<OpeningHour> getOpeningHours() {
        return openingHours;
    }
}
