package org.madrien.serialization;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class JSONClass {

    @SerializedName("data")
    private List<Datum> data = null;

    public List<Datum> getData() {
        return data;
    }
}