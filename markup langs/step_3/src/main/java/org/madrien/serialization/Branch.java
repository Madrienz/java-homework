package org.madrien.serialization;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Branch {

    @SerializedName("Identification")
    private String identification;
	
    @SerializedName("SequenceNumber")
    private String sequenceNumber;
	
    @SerializedName("Name")
    private String name;
	
    @SerializedName("Type")
    private String type;
	
    @SerializedName("CustomerSegment")
    private List<String> customerSegment = null;
	
    @SerializedName("Accessibility")
    private List<String> accessibility = null;
	
    @SerializedName("OtherServiceAndFacility")
    private List<OtherServiceAndFacility> otherServiceAndFacility = null;
	
    @SerializedName("Availability")
    private Availability availability;
	
    @SerializedName("ContactInfo")
    private List<ContactInfo> contactInfo = null;
	
    @SerializedName("PostalAddress")
    private PostalAddress postalAddress;

    @Override
    public String toString() {
        return "Branch{" +
                "identification='" + identification + '\'' +
                ", sequenceNumber='" + sequenceNumber + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", customerSegment=" + customerSegment +
                ", accessibility=" + accessibility +
                ", otherServiceAndFacility=" + otherServiceAndFacility +
                ", availability=" + availability +
                ", contactInfo=" + contactInfo +
                ", postalAddress=" + postalAddress +
                '}';
    }

    public String getIdentification() {
        return identification;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<String> getCustomerSegment() {
        return customerSegment;
    }

    public List<String> getAccessibility() {
        return accessibility;
    }

    public List<OtherServiceAndFacility> getOtherServiceAndFacility() {
        return otherServiceAndFacility;
    }

    public Availability getAvailability() {
        return availability;
    }

    public List<ContactInfo> getContactInfo() {
        return contactInfo;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }
}
