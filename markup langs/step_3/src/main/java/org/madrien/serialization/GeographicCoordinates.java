package org.madrien.serialization;

import com.google.gson.annotations.SerializedName;

public class GeographicCoordinates {

    @SerializedName("Latitude")
    private String latitude;
	
    @SerializedName("Longitude")
    private String longitude;

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
