package org.madrien.serialization;

import com.google.gson.annotations.SerializedName;

public class Availability {

    @SerializedName("StandardAvailability")
    private StandardAvailability standardAvailability;

    public StandardAvailability getStandardAvailability() {
        return standardAvailability;
    }
}
