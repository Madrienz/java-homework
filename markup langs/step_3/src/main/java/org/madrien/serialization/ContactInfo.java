package org.madrien.serialization;

import com.google.gson.annotations.SerializedName;

public class ContactInfo {

    @SerializedName("ContactType")
    private String contactType;
	
    @SerializedName("ContactContent")
    private String contactContent;

    public String getContactType() {
        return contactType;
    }

    public String getContactContent() {
        return contactContent;
    }
}
