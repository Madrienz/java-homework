package org.madrien.serialization;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostalAddress {

    @SerializedName("AddressLine")
    private List<String> addressLine = null;
	
    @SerializedName("TownName")
    private String townName;
	
    @SerializedName("CountrySubDivision")
    private List<String> countrySubDivision = null;
	
    @SerializedName("Country")
    private String country;
	
    @SerializedName("PostCode")
    private String postCode;
	
    @SerializedName("GeoLocation")
    private GeoLocation geoLocation;

    public List<String> getAddressLine() {
        return addressLine;
    }

    public String getTownName() {
        return townName;
    }

    public List<String> getCountrySubDivision() {
        return countrySubDivision;
    }

    public String getCountry() {
        return country;
    }

    public String getPostCode() {
        return postCode;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }
}
