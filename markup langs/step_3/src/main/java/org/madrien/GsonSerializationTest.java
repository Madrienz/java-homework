package org.madrien;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import org.madrien.serialization.*;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GsonSerializationTest {
    public static void main(String[] args) {
        Path input = Paths.get("src/main/java/org/madrien/example.json");
        Path output = Paths.get("src/main/java/org/madrien/serialized.json");

        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting().serializeNulls();
        Gson gson = builder.create();
        JSONClass object = null;
        try (JsonReader reader = new JsonReader(new FileReader(input.toFile()))) {
            object = gson.fromJson(reader, JSONClass.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Datum datum = object.getData().get(0);
        Brand brand = datum.getBrand().get(0);
        System.out.println(brand.getBrandName());
        Branch branch = brand.getBranch().get(0);
        System.out.println(branch);
        brand.setBrandName("Madrien Bank");

        try (FileWriter writer = new FileWriter(output.toFile())) {
            if(Files.notExists(output)) {
                Files.createFile(output);
            }

            gson.toJson(object, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //read changed value
        try (JsonReader reader = new JsonReader(new FileReader(output.toFile()))) {
            object = gson.fromJson(reader, JSONClass.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        datum = object.getData().get(0);
        brand = datum.getBrand().get(0);
        System.out.println(brand.getBrandName());
    }
}
