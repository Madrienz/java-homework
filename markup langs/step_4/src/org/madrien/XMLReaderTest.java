package org.madrien;

import javax.xml.parsers.*;
import javax.xml.*;
import java.io.*;
import org.xml.sax.helpers.*;
import org.xml.sax.*;

public class XMLReaderTest {
	public static void main(String[] args) {
		SAXParserFactory factory = SAXParserFactory.newInstance(); //newDefaultInstance in java 9+
		SAXParser parser = null;
		MyHandler contentHandler = new MyHandler();
		try{	
			parser = factory.newSAXParser();
			parser.parse(new File("TestXML.xml"), new MyHandler());
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public static class MyHandler extends DefaultHandler {
		char[] array;
		boolean isFound = false;
		
		public void startElement(String uri, String localName, String qName, Attributes atts) {
			if(atts.getValue("OBS_STATUS") != null) {
				if(atts.getValue("OBS_STATUS").equals("M")) {
					System.out.println("Found: " + qName + 
					" " + atts.getQName(0) + "=" + atts.getValue(0) + 
					" " + atts.getQName(1) + "=" + atts.getValue(1) + 
					" " + atts.getQName(2) + "=" + atts.getValue(2));
					isFound = true;
				}
			}
		}
		
		public void endDocument() throws SAXException {
			if(isFound == false) {
				throw new SAXException("No element with an attribute OBS_STATUS = 'M' found.");
			}
		}
		
		public void characters(char[] ch, int start, int length) {
			array = ch;
		}
	}
}