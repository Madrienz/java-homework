package org.madrien;

import javax.xml.parsers.*; 
import org.xml.sax.SAXException; 
import org.xml.sax.helpers.*;
import org.w3c.dom.*;
import java.io.*;

public class DOMTest {
	public static void main(String[] args) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document doc = null;
		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(new File("TestXML.xml"));
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}	
			
		NodeList list = doc.getElementsByTagName("ns14:Obs");
		
		Node node = null;
		NamedNodeMap map = null;
		Node attr = null;
		for (int i = 0; i < list.getLength(); i++) {
			node = list.item(i);
			map = node.getAttributes();
			attr = map.getNamedItem("OBS_STATUS");
			if(attr.getNodeValue().equals("M")) {
				System.out.println("Found: " + attr.getNodeName() + "=" + attr.getNodeValue());
			}
		}
	}
}