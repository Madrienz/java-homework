package org.madrien;

import javax.json.Json;
import javax.json.JsonValue;
import javax.json.stream.JsonParser;
import static javax.json.stream.JsonParser.Event;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class JsonReaderTest {
    public static void main(String[] args) {
	    try (InputStream is = new FileInputStream(new File("src/org/madrien/example.json"));
             JsonParser parser = Json.createParser(is)) {

            while(parser.hasNext()) {
                Event event = parser.next();
                if(event == Event.KEY_NAME) {
                    String key = parser.getString();
                    if(key.equals("Brand")) {
                        System.out.print("Node with name " + key + " has type ");
                        event = parser.next();
                        JsonValue value = parser.getValue();
                        System.out.print(value.getValueType());
                        System.out.print(" and value: " + value);
                    }
                }
            }

        } catch (IOException e) {
	        e.printStackTrace();
        }
    }
}
