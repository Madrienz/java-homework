#Step_1 answers

1.  My favourite option is probably Streams API, because once you get used to lambdas and method references,
    I believe streams give you the most powerful tool for working with collections. The downside of it is the 
    steep learning curve - you need to work with them a lot to remember all different methods, their nuances, and 
    functional interfaces. But when you need to do a lot of operations with a collection, you just write one line of code
    for an operation and the result is readable and easy maintainable code.
2.  For-each loop is my second best option for working with collections. It's pretty straightforward, nothing much to learn 
    or remember. If you need to do a simple operation or two - you pick for-each loop. It's easy to read and to
    understand, unless you need to do multiple checks and operations on an element. In that case stacked 
    if-statements or even other for-loops become much less readable.
3.  I haven't used iterators much, they are alright, but I prefer other tools.