package org.madrien;

import java.util.*;
import java.util.stream.*;

public class FilterOrders {
    public static void main(String[] args) {
		
		//create a list and fill it with completed orders with price < 5000$
		List<Order> list = new ArrayList<>();
		for(int i=0; i<100; i++) {
			list.add(new Order(OrderStatus.COMPLETED, (int) (Math.random() * 5000)));
		}
		
		System.out.println("Using streams");
		List<Order> resultList = list.stream()
			.filter(order -> order.getPrice() < 2000)
			.collect(Collectors.toList);
		System.out.println(resultList);
		resultList.clear();	
		
		System.out.println("Using for-each");	
		for(Order order: list) {
			if(order.getPrice() < 300) {
				resultList.add(order);
			}
		}
		System.out.println(resultList);
		resultList.clear();
		
		System.out.println("Using iterator");
		for(Iterator<Order> iterator = list.iterator(); iterator.hasNext();) {
			Order order = null;
			if((order = iterator.next()).getPrice() > 4000)
				resultList.add(order);
		}
		System.out.println(resultList);
    }


    public enum OrderStatus {
        NOT_STARTED, PROCESSING, COMPLETED
    }

    public static class Order {
        public final OrderStatus status; 
		private int price;

        public Order(OrderStatus status, int price) {
            this.status = status;
			this.price = price;
        }

        public OrderStatus getStatus() {
            return status;
        }
		
		public int getPrice() {
			return price;
		}
		
		public String toString() {
			return price + "";
		}
    }
}