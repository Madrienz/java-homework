package org.madrien;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class TestBenchmark {
    public static void main(String[] args) {
        List<Integer> fighter1 = new ArrayList<>();
        List<Integer> fighter2 = new Vector<>();

        Benchmark.testAddingElementsToTheEnd(fighter1, fighter2);
        Benchmark.testAddingElementsToArbitraryPosition(fighter1, fighter2);
        Benchmark.testGettingElementAtIndex(fighter1, fighter2);
        Benchmark.testRemovingElementAtIndex(fighter1, fighter2);
    }
}
