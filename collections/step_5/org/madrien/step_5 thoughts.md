I've picked ArrayList vs Vector and tested their ability to:

    - add an element to the end of the list;
    - add an element to an arbitrary index;
    - get element;
    - remove element;
    
I did a few tests, here are the results of the last one:

    ArrayList// Avg time for adding 5kk elements to the end: 176.1 ms
    Vector// Avg time for adding 5kk elements to the end: 241.0 ms 
    ArrayList// Avg time for adding 200k elements to the arbitrary index: 1924.6 ms
    Vector// Avg time for adding 200k elements to the arbitrary index: 1931.8 ms 
    ArrayList// Avg time for getting 5kk elements: 102.5 ms 
    Vector// Avg time for getting 5kk elements: 159.4 ms 
    ArrayList// Avg time for removing 1k elements: 1344.3 ms 
    Vector// Avg time for removing 1k elements: 1384.3 ms
    
Tests are pretty consistent, though I'm not gonna say they are very accurate.
I could tweak few numbers here and there, make more tests for a better average, 
but I think this should be enough. 
We can say that both ArrayList and Vector are very good at adding elements to the
end of the list and getting an element from any position. ArrayList is slightly 
better than Vector in terms of speed. But Vector is thread-safe, so it pays the price 
in speed. 