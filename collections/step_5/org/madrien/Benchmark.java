package org.madrien;

import java.util.*;

public class Benchmark {

    private static long timeBefore;
    private static long timeAfter;
    private static List<Long> result1 = new ArrayList<>();
    private static List<Long> result2 = new ArrayList<>();

    public static double avg(List<Long> list) {
        OptionalDouble avg = list.stream().mapToLong(o -> o.longValue()).average();
        return avg.getAsDouble();
    }

    public static void testAddingElementsToTheEnd(List<Integer> fighter1, List<Integer> fighter2) {
        //repeat 10 times for better results
        for(int j = 0; j < 10; j++) {
            timeBefore = System.currentTimeMillis();
            for (int i = 0; i < 5000000; i++) {
                fighter1.add((int) (Math.random() * 5000));
            }
            timeAfter = System.currentTimeMillis();
            System.out.println(fighter1.getClass().getSimpleName() + " time: " + (timeAfter - timeBefore));
            result1.add(timeAfter - timeBefore);

            timeBefore = System.currentTimeMillis();
            for (int i = 0; i < 5000000; i++) {
                fighter2.add((int) (Math.random() * 5000));
            }
            timeAfter = System.currentTimeMillis();
            System.out.println(fighter2.getClass().getSimpleName() + " time: " + (timeAfter - timeBefore));
            result2.add(timeAfter - timeBefore);

            fighter1.clear();
            fighter2.clear();
        }
        System.out.println(fighter1.getClass().getSimpleName() + ": Avg time for adding element to the end: " + avg(result1));
        System.out.println(fighter2.getClass().getSimpleName() + ": Avg time for adding element to the end: " + avg(result2));
        result1.clear();
        result2.clear();
    }

    public static void testAddingElementsToArbitraryPosition(List<Integer> fighter1, List<Integer> fighter2) {
        //repeat 10 times for better results
        for(int j = 0; j < 10; j++) {
            timeBefore = System.currentTimeMillis();
            for (int i = 0; i < 200000; i++) {
                fighter1.add((fighter1.size() / 2), (int) (Math.random() * 5000));
            }
            timeAfter = System.currentTimeMillis();
            System.out.println(fighter1.getClass().getSimpleName() + " time: " + (timeAfter - timeBefore));
            result1.add(timeAfter - timeBefore);

            timeBefore = System.currentTimeMillis();
            for (int i = 0; i < 200000; i++) {
                fighter2.add((fighter2.size() / 2), (int) (Math.random() * 5000));
            }
            timeAfter = System.currentTimeMillis();
            System.out.println(fighter2.getClass().getSimpleName() + " time: " + (timeAfter - timeBefore));
            result2.add(timeAfter - timeBefore);

            fighter1.clear();
            fighter2.clear();
        }
        System.out.println(fighter1.getClass().getSimpleName() + ": Avg time for adding element to the arbitrary index: " + avg(result1));
        System.out.println(fighter2.getClass().getSimpleName() + ": Avg time for adding element to the arbitrary index: " + avg(result2));
        result1.clear();
        result2.clear();
    }

    public static void testGettingElementAtIndex(List<Integer> fighter1, List<Integer> fighter2) {
        //fill before test
        for(int i=0; i<3000000; i++) {
            fighter1.add((int) (Math.random() * 5000));
        }

        for(int i=0; i<3000000; i++) {
            fighter2.add((int) (Math.random() * 5000));
        }

        //repeat 10 times for better results
        for(int j = 0; j < 10; j++) {
            timeBefore = System.currentTimeMillis();
            for (int i = 0; i < 5000000; i++) {
                fighter1.get((int) (Math.random() * 99999));
            }
            timeAfter = System.currentTimeMillis();
            System.out.println(fighter1.getClass().getSimpleName() + " time: " + (timeAfter - timeBefore));
            result1.add(timeAfter - timeBefore);

            timeBefore = System.currentTimeMillis();
            for (int i = 0; i < 5000000; i++) {
                fighter2.get((int) (Math.random() * 99999));
            }
            timeAfter = System.currentTimeMillis();
            System.out.println(fighter2.getClass().getSimpleName() + " time: " + (timeAfter - timeBefore));
            result2.add(timeAfter - timeBefore);
        }
        fighter1.clear();
        fighter2.clear();

        System.out.println(fighter1.getClass().getSimpleName() + ": Avg time for getting element: " + avg(result1));
        System.out.println(fighter2.getClass().getSimpleName() + ": Avg time for getting element: " + avg(result2));
        result1.clear();
        result2.clear();
    }

    public static void testRemovingElementAtIndex(List<Integer> fighter1, List<Integer> fighter2) {
        //fill before test
        for(int i=0; i<3000000; i++) {
            fighter1.add((int) (Math.random() * 5000));
        }

        for(int i=0; i<3000000; i++) {
            fighter2.add((int) (Math.random() * 5000));
        }

        //repeat 10 times for better results
        for(int j = 0; j < 10; j++) {
            timeBefore = System.currentTimeMillis();
            for (int i = 0; i < 1000; i++) {
                fighter1.remove((int) (Math.random() * 2900000));
            }
            timeAfter = System.currentTimeMillis();
            System.out.println(fighter1.getClass().getSimpleName() + " time: " + (timeAfter - timeBefore));
            result1.add(timeAfter - timeBefore);

            timeBefore = System.currentTimeMillis();
            for (int i = 0; i < 1000; i++) {
                fighter2.remove((int) (Math.random() * 2900000));
            }
            timeAfter = System.currentTimeMillis();
            System.out.println(fighter2.getClass().getSimpleName() + " time: " + (timeAfter - timeBefore));
            result2.add(timeAfter - timeBefore);
        }
        fighter1.clear();
        fighter2.clear();

        System.out.println(fighter1.getClass().getSimpleName() + ": Avg time for removing element: " + avg(result1));
        System.out.println(fighter2.getClass().getSimpleName() + ": Avg time for removing element: " + avg(result2));
        result1.clear();
        result2.clear();
    }
}