package org.madrien;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SimpleCache<T> {
	/** map for storing values */
	private Map<String, T> valueMap;
	/** map for storing expiring time of values */
	private Map<String, Long> expireMap;
	/** default value for expiring time used if not specified otherwise */
	private final long defaultExpire;
	/** daemon thread for cleaning maps from expired values */
	private final ScheduledExecutorService cleaningThread;
	
	/** construct an object with default expiration time of 10 seconds */
	public SimpleCache() {
		this(10);
	}
	
	/** construct an object with custom expiration time */
	public SimpleCache(long defaultExpire) {
		this.valueMap = new ConcurrentHashMap<>();
		this.expireMap = new ConcurrentHashMap<>();
		this.defaultExpire = defaultExpire;
		this.cleaningThread = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread thread = new Thread(r);
				thread.setDaemon(true);
				return thread;
			}
		});
		cleaningThread.scheduleWithFixedDelay(() -> {
			for(String name : expireMap.keySet()) {
				if(System.currentTimeMillis() > expireMap.get(name)) {
					valueMap.remove(name);
					expireMap.remove(name);
				}
			}
			System.out.println("Job done");
		}, this.defaultExpire / 2, this.defaultExpire, TimeUnit.SECONDS);
	}
	
	/** getter for expiration time of cache object */
	public long getExpire() {
		return defaultExpire;
	}
	
	/** method for putting values in the cache with default expiration time */
	public void put(String name, T obj) {
		this.put(name, obj, defaultExpire);
	}
	
	/** method for putting values in the cache with custom expiration time */
	public void put(String name, T obj, long expireTime) {
		valueMap.put(name, obj);
		expireMap.put(name, System.currentTimeMillis() + expireTime * 1000);
	}
	
	/** method for getting values from the cache */
	public T get(String name) {
		Long expireTime = this.expireMap.get(name);
		if(expireTime == null) return null;
		if(System.currentTimeMillis() > expireTime) {
			valueMap.remove(name);
			expireMap.remove(name);
			return null;
		}
		return valueMap.get(name);
	}
}