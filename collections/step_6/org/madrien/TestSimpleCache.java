package org.madrien;

public class TestSimpleCache {
	public static void main(String[] args) {
		SimpleCache<Integer> cache = new SimpleCache<>();
		cache.put("Annie", 3000);
		cache.put("John", 5000) ;
		cache.put("Ashley", 6000);
		
		Integer a = cache.get("Annie");
		Integer b = cache.get("John");
		Integer c = cache.get("Ashley");
		
		System.out.println("Annie: "+a);
		System.out.println("John: "+b);
		System.out.println("Ashley: "+c);
		
		try{
			Thread.sleep(10000);
		} catch (Exception e) {}
		
		a = cache.get("Annie");
		b = cache.get("John");
		c = cache.get("Ashley");
		
		System.out.println("Annie: "+a);
		System.out.println("John: "+b);
		System.out.println("Ashley: "+c);
	}
}