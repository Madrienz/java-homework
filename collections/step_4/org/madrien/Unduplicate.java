package org.madrien;

import java.util.*;

public class Unduplicate {
	public static <E> Set<E> removeDuplicates(List<E> list) {
		Set set = new HashSet<E>();
		set.addAll(list);
		return set;
	}
	
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		
		for(int i=0; i<100; i++) {
			list.add((int) (Math.random() * 10));
		}
		
		System.out.println(Unduplicate.removeDuplicates(list));
	}
}