I've made a singleton process queue for working with requests because I think it will be easier
to maintain all requests and their order that way. That works good if we do not want to lose any request
and process them in order of their arrival. Implementations can change if we want to process
the latest requests first, and/or drop some old unprocessed yet requests. 

Since it will be accessed by multiple threads I've made a `ConcurrentLinkedQueue`. In my test queue 
is filled with static amount of requests and program ends when queue becomes empty.
For a more realistic case where requests always come and get processed continually `LinkedBlockingQueue` 
would probably be a better option since it can wait for element to become available for processing
or for free space when queue is filled.