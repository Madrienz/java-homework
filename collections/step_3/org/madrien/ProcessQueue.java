package org.madrien;

import java.util.*;
import java.util.concurrent.*;

public class ProcessQueue {
	private static final Queue<Request> instance = new ConcurrentLinkedQueue<Request>();
		
	public static Queue<Request> getInstance() {
		return instance;
	}
	
	private ProcessQueue() {}
}

class Request {	
	private boolean isProcessed = false;
	
	public void setIsProcessed(boolean b) {
		isProcessed = b;
	}
	
	public boolean isProcessed() {
		return isProcessed;
	}
	
	public void processRequest() {
		this.setIsProcessed(true);
	}
}
	