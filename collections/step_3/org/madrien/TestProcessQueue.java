package org.madrien;

import java.util.*;
import java.util.concurrent.*;

public class TestProcessQueue {
	public static void main(String[] args) {
		Queue<Request> queue = ProcessQueue.getInstance();
		
		for(int i=0; i<10000; i++) {
			queue.offer(new Request());
		}
		
		ExecutorService service = null;
		try {
			service = Executors.newFixedThreadPool(3);
			long timeBefore = System.currentTimeMillis();
			
			
			for(int i=0; i<3; i++) {
				Future<?> result = service.submit(() -> { 
				    Request request = null;
					while((request = queue.poll()) != null) {
				        //do something with processed request...
						request.processRequest();
				        System.out.println(request.isProcessed()); 															
					}
					System.out.println("Queue is empty");
					long timeAfter = System.currentTimeMillis();
				    System.out.println(timeAfter - timeBefore); });
			}
		} finally {
			if(service != null) service.shutdown();
		}
	}
}