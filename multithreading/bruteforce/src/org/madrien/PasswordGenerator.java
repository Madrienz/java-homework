package org.madrien;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class PasswordGenerator {
    private static BlockingQueue<String> possiblePasswords = new LinkedBlockingQueue<>(20000);
    /** this thread fills the queue */
    private static ExecutorService thread = Executors.newSingleThreadExecutor(new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(BruteForcer.threadGroup, r);
            return thread;
        }
    });

    public static BlockingQueue<String> getInstance() {
        return possiblePasswords;
    }

    public static ExecutorService getThread() {
        return thread;
    }

    /** recursive method for filling the blocking queue with possible passwords */
    static void printAllKLength(char[] set, int k) {
        int n = set.length;
        printAllKLengthRec(set, "", n, k);
    }

    static void printAllKLengthRec(char[] set, String prefix, int n, int k) {
        if (k == 0) {
            //System.out.println(prefix);
            try {
                possiblePasswords.offer(prefix, 100, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            return;
        }

        for (int i = 0; i < n; i++) {
            if(!Thread.currentThread().isInterrupted()) {
                String newPrefix = prefix + set[i];
                printAllKLengthRec(set, newPrefix, n, k - 1);
            } else {
                break;
            }
        }
    }

    public static Callable<String> fillPossiblePasswords = () -> {
        char[] set = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
             for (int i = 1; i <= 8; i++) {
                 printAllKLength(set, i);
             }
        return "Queue filled.";
    };

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Future<String> fillResult = thread.submit(fillPossiblePasswords);
        List<Future<String>> listOfResults = new ArrayList<>();
        for(int i = 0; i < 32; i++) {
            listOfResults.add(BruteForcer.getThreads().submit(BruteForcer.confirmHash));
        }

        System.out.println(fillResult.get());
        for(int i = 0; i < 32; i++) {
            System.out.println(listOfResults.get(i).get());
        }
        thread.shutdown();
        BruteForcer.getThreads().shutdown();
    }
}