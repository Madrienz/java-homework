package org.madrien;

import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.*;

public class BruteForcer {
    public static ThreadGroup threadGroup = new ThreadGroup("The Gang");
    /** this thread pool converts possible passwords from queue to hash codes and compares them with our hash */
    private static ExecutorService threads = Executors.newFixedThreadPool(32, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(threadGroup, r);
            return thread;
        }
    });

    public static ExecutorService getThreads() {
        return threads;
    }

    public static Callable<String> confirmHash = () -> {
        Hasher hasher = null;
        String lastProcessedPassword = "";
        String hash = null;
        int counter = 0;
        while(!Thread.currentThread().isInterrupted()) {
            hasher = Hashing.md5().newHasher();
            try {
                lastProcessedPassword = PasswordGenerator.getInstance().poll(100, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            hasher.putString(lastProcessedPassword, StandardCharsets.UTF_8);
            hash = hasher.hash().toString().toUpperCase();
            //System.out.println("Checking " + lastProcessedPassword);
            if (hash.equals("DDC4035FF6451F85BB796879E9E5EA49")) {
                System.out.println("The password is: " + lastProcessedPassword);
                counter++;
                Thread.currentThread().getThreadGroup().interrupt();
            }
            counter++;
        }
        return "Finished exucuting with " + counter + " tries and last try is: " + lastProcessedPassword;
    };
}
