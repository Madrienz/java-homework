package org.madrien;

public class Bank {
	private int moneyAmount;
	
	public Bank(int moneyAmount) {
		this.moneyAmount = moneyAmount;
	}
	
	public void transferMoney(int amount) {
		moneyAmount -= amount;
		System.out.println(moneyAmount + "$ left.");
		if(moneyAmount < 0)
			throw new RuntimeException("Negative money!");
	}
	
	public boolean hasMoney(int amount) {
		return (amount <= moneyAmount);
	}
}

class BankUser {
	private Bank bank;
	
	public BankUser(Bank bank) {
		this.bank = bank;
	}
	
	public boolean withdraw() {
		synchronized(bank) {
			if(bank.hasMoney(500)) {
				bank.transferMoney(500);
				System.out.println("Success.");
				return true;
			} else {
				System.out.println("No money in the bank.");
				return false;
			}
		}
	}
}