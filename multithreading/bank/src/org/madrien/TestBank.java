package org.madrien;

import java.util.concurrent.*;

public class TestBank {
	public static void main(String[] args) {
		Bank bank = new Bank(1_000);
		ExecutorService service = null;
		
		try {
			service = Executors.newFixedThreadPool(10);
			for(int i=0; i<10; i++) {
				service.execute(() -> {
					BankUser user = new BankUser(bank);
					boolean b = true;
					while(b) {
						b = user.withdraw();
					}
				});
			}
		} finally {
			if(service != null) service.shutdown();
		}
	}
}