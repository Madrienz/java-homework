package org.madrien;

import org.junit.*;
import org.postgresql.util.PSQLException;

import java.sql.*;

import static org.junit.Assert.assertEquals;

public class JDBCTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        DBHelper.createDatabase();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        DBHelper.dropDatabase();
    }

    @Before
    public void setUp() throws Exception {
        TableHelper.createTables();
        TableHelper.populateTables();
        TableHelper.addTriggerAndCallable();
    }

    @After
    public void tearDown() throws Exception {
        TableHelper.dropTables();
    }

    @Test
    public void testSelect() {
        try (Connection conn = TableHelper.getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "SELECT * FROM songs;";
            ResultSet rs = stmt.executeQuery(query);
            System.out.println("Selecting songs table.");
            while(rs.next()) {
                System.out.println(rs.getInt("song_id")
                                + " " + rs.getInt("artist_id")
                                + " " + rs.getString("name")
                                + " " + rs.getInt("album_id")
                                + " " + rs.getString("duration"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInnerJoins() {
        try (Connection conn = TableHelper.getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "SELECT "
                    + "first_name, "
                    + "last_name, "
                    + "songs.name AS song_name, "
                    + "albums.name AS album_name, "
                    + "year "
                    + "FROM songs INNER JOIN artists "
                    + "ON songs.artist_id = artists.artist_id "
                    + "INNER JOIN albums "
                    + "ON songs.album_id = albums.album_id;";
            ResultSet rs = stmt.executeQuery(query);
            System.out.println("INNER JOIN of all tables.");
            while(rs.next()) {
                System.out.println(rs.getString("first_name")
                            + " " + rs.getString("last_name")
                            + ", " + rs.getString("song_name")
                            + ", " + rs.getString("album_name")
                            + ", " + rs.getInt("year"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testLeftJoins() {
        try (Connection conn = TableHelper.getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "SELECT "
                    + "first_name, "
                    + "last_name, "
                    + "songs.name AS song_name, "
                    + "albums.name AS album_name, "
                    + "year "
                    + "FROM songs LEFT JOIN artists "
                    + "ON songs.artist_id = artists.artist_id "
                    + "LEFT JOIN albums "
                    + "ON songs.album_id = albums.album_id;";
            ResultSet rs = stmt.executeQuery(query);
            System.out.println("LEFT JOIN of all tables.");
            while(rs.next()) {
                System.out.println(rs.getString("first_name")
                        + " " + rs.getString("last_name")
                        + ", " + rs.getString("song_name")
                        + ", " + rs.getString("album_name")
                        + ", " + rs.getInt("year"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPreparedStatement() {
        String query = "UPDATE albums SET genre = ? WHERE genre = ?;";
        try (Connection conn = TableHelper.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(query);
             Statement stmt = conn.createStatement()) {

            pstmt.setString(1, "Alternative/Indie");
            pstmt.setString(2, "Indie");
            int rowsAffected = pstmt.executeUpdate();
            assertEquals(2, rowsAffected);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCallableStatement() {
        String query = "{call show_favorites()}";
        try (Connection conn = TableHelper.getConnection();
             CallableStatement cstmt = conn.prepareCall(query)) {

            ResultSet rs = cstmt.executeQuery();
            System.out.println("Using show_favorites() callable procedure.");
            while(rs.next()) {
                System.out.println(rs.getString("first_name")
                        + " " + rs.getString("last_name")
                        + ", " + rs.getString("song_name")
                        + ", " + rs.getString("album_name")
                        + ", " + rs.getBoolean("favorite"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDeleteWithTrigger() {
        try (Connection conn = TableHelper.getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "DELETE FROM songs WHERE name = 'Risk';";
            int rowsAffected = stmt.executeUpdate(query);
            assertEquals(1, rowsAffected);

            query = "DELETE FROM songs WHERE name = 'The Arsonist';";
            rowsAffected = stmt.executeUpdate(query);
            assertEquals(0, rowsAffected);
        } catch (PSQLException e) {
            System.out.println("Error, that supposed to happen: ");
            System.out.println(e.getMessage()); //caught trigger exception
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMaxFunc() {
        try (Connection conn = TableHelper.getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "SELECT "
                    + "MAX(year) AS year "
                    + "FROM albums;";
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()) {
                assertEquals(2018, rs.getInt("year"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTransactions() {
        try (Connection conn = TableHelper.getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "BEGIN; "
                    + "INSERT INTO songs (name, duration) "
                    + "VALUES ('Ares', '5:50');";
            int rowsAffected = stmt.executeUpdate(query);
            assertEquals(0, rowsAffected);

            query = "UPDATE songs SET duration = '6:00' WHERE name = 'Ares'; "
                    + "COMMIT;";
            rowsAffected = stmt.executeUpdate(query);
            assertEquals(1, rowsAffected);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}