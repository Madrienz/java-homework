package org.madrien;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBHelper {
    public static final String USERNAME = "postgres";
    public static final String PASSWORD = "";

    public static void createDatabase() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

     /*   Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter your username for accessing PostgreSQL (preferably 'postgres'): ");
        username = scanner.nextLine();
        System.out.println("Please enter your password: ");
        password = scanner.nextLine();  */

        try (Connection conn = DriverManager
                .getConnection("jdbc:postgresql://localhost/", USERNAME, PASSWORD);
             Statement stmt = conn.createStatement()) {

            String createdb = "CREATE DATABASE homeworkdb;";
            stmt.executeUpdate(createdb);
            System.out.println("Database 'homeworkdb' was created successfully.");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void dropDatabase() {
        try (Connection conn = DriverManager
                .getConnection("jdbc:postgresql://localhost/", USERNAME, PASSWORD);
             Statement stmt = conn.createStatement()) {
            String query = "DROP DATABASE homeworkdb;";
            stmt.executeUpdate(query);
            System.out.println("The database 'homeworkdb' successfully deleted.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
