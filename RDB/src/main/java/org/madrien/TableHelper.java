package org.madrien;

import java.sql.*;

public class TableHelper {

    public static Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:postgresql://localhost/homeworkdb", DBHelper.USERNAME, DBHelper.PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static void createTables() {
        try (Connection conn = getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "CREATE TABLE artists "
                    + "("
                    + "artist_id SERIAL,"
                    + "first_name VARCHAR(20),"
                    + "last_name VARCHAR(30),"
                    + "PRIMARY KEY(artist_id)"
                    + ");";
            stmt.executeUpdate(query);
            query = "CREATE TABLE albums "
                    + "("
                    + "album_id SERIAL,"
                    + "name VARCHAR(100),"
                    + "year INTEGER,"
                    + "num_of_tracks INTEGER,"
                    + "duration VARCHAR(6),"
                    + "genre VARCHAR(50),"
                    + "PRIMARY KEY(album_id)"
                    + ");";
            stmt.executeUpdate(query);
            query = "CREATE TABLE songs "
                    + "("
                    + "song_id SERIAL,"
                    + "artist_id INTEGER,"
                    + "name VARCHAR(100),"
                    + "album_id INTEGER,"
                    + "duration VARCHAR(5),"
                    + "favorite BOOLEAN DEFAULT FALSE,"
                    + "FOREIGN KEY(album_id) REFERENCES albums (album_id),"
                    + "FOREIGN KEY(artist_id) REFERENCES artists (artist_id),"
                    + "PRIMARY KEY (song_id)"
                    + ");";
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void populateTables() {
        try (Connection conn = getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "INSERT INTO artists "
                    + "VALUES (DEFAULT, 'Maynard', 'Keenan'),"
                    + "       (DEFAULT, 'Alex', 'Turner'),"
                    + "       (DEFAULT, 'Damon', 'Albarn'),"
                    + "       (DEFAULT, 'Thomas', 'Erak'),"
                    + "       (DEFAULT, 'Emily', 'Haines');";
            stmt.executeUpdate(query);
            query = "INSERT INTO albums "
                    + "VALUES (DEFAULT, 'Art of Doubt', 2018, 12, '52:16', 'Alternative'),"
                    + "       (DEFAULT, 'Humbug', 2009, 10, '39:15', 'Indie'),"
                    + "       (DEFAULT, 'Demon Days', 2005, 15, '51:38', 'Alternative/Pop'),"
                    + "       (DEFAULT, 'Manipulator', 2007, 12, '47:21', 'Hardcore'),"
                    + "       (DEFAULT, 'Money Shot', 2016, 10, '51:27', 'Indie');";
            stmt.executeUpdate(query);
            query = "INSERT INTO songs "
                    + "VALUES (DEFAULT, 1, 'The Arsonist', 5, '4:45', 'TRUE'),"
                    + "       (DEFAULT, 2, 'Fire And The Thud', 2, '3:57', 'TRUE'),"
                    + "       (DEFAULT, 5, 'Risk', 1, '5:24', 'FALSE'),"
                    + "       (DEFAULT, 4, 'Sledgehammer', 4, '6:01', 'TRUE'),"
                    + "       (DEFAULT, 3, 'Feel Good Inc', 3, '3:42', 'FALSE');";
            stmt.executeUpdate(query);

            //add some songs without artist and album for LEFT JOIN demonstration
            query = "INSERT INTO songs (name, duration) "
                    + "VALUES ('Garage Queen', '2:51'),"
                    + "       ('Time Is Running Out', '3:57');";
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addTriggerAndCallable() {
        try (Connection conn = getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "CREATE OR REPLACE FUNCTION show_favorites() RETURNS TABLE( "
                    + "first_name VARCHAR(20),"
                    + "last_name VARCHAR(30),"
                    + "song_name VARCHAR(100),"
                    + "album_name VARCHAR(50), "
                    + "favorite BOOLEAN) AS "
                    + "$$ "
                    + "SELECT "
                    + "artists.first_name,"
                    + "artists.last_name,"
                    + "songs.name,"
                    + "albums.name, "
                    + "songs.favorite "
                    + "FROM songs INNER JOIN artists "
                    + "ON songs.artist_id = artists.artist_id "
                    + "INNER JOIN albums "
                    + "ON songs.album_id = albums.album_id "
                    + "WHERE songs.favorite = TRUE; "
                    + "$$ "
                    + "LANGUAGE sql;";
            stmt.executeUpdate(query);
            query = "CREATE OR REPLACE FUNCTION check_favorite() RETURNS TRIGGER AS "
                    + "$$ "
                    + "BEGIN "
                    + "     IF OLD.favorite = TRUE THEN "
                    + "         RAISE EXCEPTION 'Cannot delete favorite song.'; "
                    + "     END IF; "
                    + "     RETURN OLD; "
                    + "END; "
                    + "$$ "
                    + "LANGUAGE plpgsql; "
                    + "CREATE TRIGGER on_delete "
                    + "BEFORE DELETE ON songs FOR EACH ROW "
                    + "EXECUTE PROCEDURE check_favorite();";
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void dropTables() {
        try (Connection conn = getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "DROP TABLE songs;";
            stmt.executeUpdate(query);

            query = "DROP TABLE albums;";
            stmt.executeUpdate(query);

            query = "DROP TABLE artists;";
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
