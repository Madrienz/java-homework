# RDB

Нужно ввести логин и пароль в 
 - public static final String USERNAME = "";
 - public static final String PASSWORD = "";

для создания и удаления бд. Они находятся в классе DBHelper.

Запускай JDBCTest.java целиком, в `@BeforeClass` создается база, в `@Before` создаются и 
заполняются таблицы, в `@After` таблицы удаляются, в `@AfterClass` удаляется база.
